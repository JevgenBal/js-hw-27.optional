const isMobile = {
	Android: () => navigator.userAgent.match(/Android/i),
	BlackBerry: () => navigator.userAgent.match(/BlackBerry/i),
	iOS: () => navigator.userAgent.match(/iPhone|iPad|iPod/i),
	Opera: () => navigator.userAgent.match(/Opera Mini/i),
	Windows: () => navigator.userAgent.match(/IEMobile/i),
	any: () => isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows(),
};

// === //
$(document).ready(function() {
  var backToTopButton = $('.back-to-top');

  backToTopButton.hide();

  $(window).scroll(function() {
    if ($(this).scrollTop() > $(window).height()) {
      backToTopButton.fadeIn();
    } else {
      backToTopButton.fadeOut();
    }
  });

  backToTopButton.click(function(e) {
    e.preventDefault();
    $('html, body').animate({ scrollTop: 0 }, 'slow');
  });
});
// === //

if (isMobile.any()) {
	$('body').addClass('_touch');

	let menuArrows = $('.menu__arrow');
	if (menuArrows.length > 0) {
			menuArrows.each(function () {
					const menuArrow = $(this);
					menuArrow.on('click', function (e) {
							menuArrow.parent().toggleClass('_active');
					});
			});
	}
} else {
	$('body').addClass('_pc');
}

const iconMenu = $('.menu__icon');
const menuBody = $('.menu__body');
if (iconMenu.length > 0) {
	iconMenu.on('click', function (e) {
			$('body').toggleClass('_lock');
			iconMenu.toggleClass('_active');
			menuBody.toggleClass('_active');
	});
}

const menuLinks = $('.menu__link[data-goto]');
if (menuLinks.length > 0) {
	menuLinks.each(function () {
			$(this).on('click', function (e) {
					e.preventDefault();

					const menuLink = $(this);
					const gotoBlock = $(menuLink.data('goto'));

					if (gotoBlock.length) {
							const gotoBlockValue = gotoBlock.offset().top - $('header').outerHeight();

							if (iconMenu.hasClass('_active')) {
									$('body').removeClass('_lock');
									iconMenu.removeClass('_active');
									menuBody.removeClass('_active');
							}

							$('html, body').animate(
									{
											scrollTop: gotoBlockValue,
									},
									800
							);
					}
			});
	});
}
